package com.gupao.rpc.zk;

public interface IRegistryCenter {
    /**
     * 注册服务
     * @param serviceName
     * @param address
     */
    void register(String serviceName,String address);
}
