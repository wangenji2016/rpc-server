package com.gupao.rpc;

import com.gupao.rpc.zk.IRegistryCenter;
import com.gupao.rpc.zk.RegistryCenterImpl;

public class LBServerDemo2 {
    public static void main(String[] args) {
        IGpHello gpHello = new GpHelloImpl();
        IRegistryCenter registryCenter = new RegistryCenterImpl();
        RpcServer rpcServer = new RpcServer("127.0.0.1:9079",registryCenter);
        rpcServer.bind(gpHello);
        rpcServer.publisher();
    }
}
