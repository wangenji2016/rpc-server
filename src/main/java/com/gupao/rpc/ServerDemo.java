package com.gupao.rpc;

import com.gupao.rpc.zk.IRegistryCenter;
import com.gupao.rpc.zk.RegistryCenterImpl;

public class ServerDemo {
    public static void main(String[] args) {
        IGpHello gpHello = new GpHelloImpl();
        IGpHello gpHello2 = new GpHelloImpl2();
        IRegistryCenter registryCenter = new RegistryCenterImpl();
        RpcServer rpcServer = new RpcServer("127.0.0.1:9079",registryCenter);
        rpcServer.bind(gpHello,gpHello2);
        rpcServer.publisher();
    }
}
