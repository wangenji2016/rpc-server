package com.gupao.rpc;

import com.gupao.rpc.annotation.RpcAnnotation;

@RpcAnnotation(value = IGpHello.class)
public class GpHelloImpl2 implements IGpHello{
    @Override
    public String sayHello(String msg) {
        return "Hello I'm 2.0, "+msg;
    }
}
