package com.gupao.rpc;

import com.gupao.rpc.zk.IRegistryCenter;
import com.gupao.rpc.zk.RegistryCenterImpl;

public class LBServerDemo1 {
    public static void main(String[] args) {
        IGpHello gpHello2 = new GpHelloImpl2();
        IRegistryCenter registryCenter = new RegistryCenterImpl();
        RpcServer rpcServer = new RpcServer("127.0.0.1:9078",registryCenter);
        rpcServer.bind(gpHello2);
        rpcServer.publisher();
    }
}
