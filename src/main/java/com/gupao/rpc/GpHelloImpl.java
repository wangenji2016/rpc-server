package com.gupao.rpc;

import com.gupao.rpc.annotation.RpcAnnotation;

@RpcAnnotation(IGpHello.class)
public class GpHelloImpl implements IGpHello{
    @Override
    public String sayHello(String msg) {
        return "Hello , "+msg;
    }
}
